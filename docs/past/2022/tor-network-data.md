# Tor network data and store it in a database

## Team

* hiro
* maybe juga
* maybe trinity-1686a

If you are interested please join the BBB room:
    https://tor.meet.coop/hir-otz-tg4-2vg
IRC:
    #tor-metrics-hw

## About this project

Many issues on metrics services come from parsing and processing snippets of tor network data collected from different sources.

We have a pipeline proposed, but so far we haven't had time to start implementing it:
https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/collector/pipeline

The idea is to build a service that parse Tor network data and sends them to a DB.

Some prior art:
https://gitlab.torproject.org/hiro/descriptorParser
https://gitlab.torproject.org/hiro/timescaledb-docker

arti parses router descriptors: https://docs.rs/tor-netdoc/0.4.1/tor_netdoc/doc/routerdesc/index.html
some crate parsing bridges descriptors: https://gitlab.torproject.org/trinity-1686a/collector-processing/-/tree/main/collector/src/descriptor/kind

Example object models in rust created with SeaORM: https://gitlab.torproject.org/juga/torm
