# Hackweek 2022 - June 27th to July 1st

 * **When**: Monday June 27th to Friday July 1st - starts on Monday at 16:00 UTC.
 * **Where**: Main channel will be #tor in irc.oftc.net . The presentation of
   projects and the demo of them will happen in
   https://tor.meet.coop/gab-dpb-9zt-7tq

This hackweek aims to promote small projects that can be round up in 5 days. We
will present the list of proposed projects on the first day of the hackweek and
other people will be able to join.

In the context of this hackweek, a project is anything related to Tor that you
can work with other people in 5 days. It could be a coding project or something
to analyze data but it could also be a cartoon or anything that do not
necessary requires coding skills.  

We encourage you all to look for collaborators to join your team before the
presentation of your project on Monday. This hackweek is for anybody interested
in Tor and not only for core contributors. You can go look for people to work
with you in other spaces other than Tor spaces (it could be social media,
mailing lists, discord, forums, etc).

During the hackweek each group will meet whenever they prefer and work on its
specific project. At the end of the week (on Friday) we will have a DEMO day
where each group will present their work. Each project will have a pad with
information about it, how to collaborate, where to meet with the rest of the
team, etc.

Each project will list this information:

* Project/team name
* Main point of contact for the project/team (timezone you are in)
* Summary of the work you want to do
* Pad for the team (the pad will have all the information about the project,
  where the team meets, planning if necessary, links, etc)
* Skills needed for the project (for example, someone who knows onionperf,
  designer, translator, code in rust, writer, etc)
* Write down how people will be able to collaborate: any specific work that can
  be done by contributors with less time than a whole week? Does it accept
  people that can only put a few hours?

## LOUNGE space

We will have the welcome/opening session on a BBB lounge room on Monday as well
as the demo day on Friday.

There is going to be a lounge space in irc for the rest of the week so teams
can give an update on their work every day.

BBB ROOM: https://tor.meet.coop/gab-dpb-9zt-7tq

IRC ROOM: #tor

## PROJECTS/GROUPS

To add yourself to a group use the group's pad please or talk with the team's
liasion.

### How to add yourself to a team

Each project has a pad with all the information related to the project, who is
coordinating it and how to collaborate. This Monday June 27th at 1600 UTC each
team will present their project. You can add yourself to the pad of the project
and go to the virtual meeting point to start collaborating.

## Questions?

Send a mail to gaba at torproject dot org or send her a message in #tor in
irc.oftc.net.

## Timeline

* Monday June 27th: Hackweek begins: [a
  session](https://tor.meet.coop/gab-dpb-9zt-7tq) that team liasons present the project.
* Friday July 1st: Hackweek ends: [team demo](https://tor.meet.coop/gab-dpb-9zt-7tq) their work.

### Monday Opening Session: 16UTC

ROOM:  https://tor.meet.coop/gab-dpb-9zt-7tq

1. Logistics.
   * main link with information
   * information about lounge every day
   * information about demo day
2. Each group present their project and people have time to ask questions.
3. Each group will go to their meeting place (BBB or irc or whatever else they
   may choose.md).
4. The  "lounge" space will be the channel #tor in irc.oftc.net for people to
   drop in during the week in case there are questions or comments, etc.

### Friday DEMO Day: 16UTC

ROOM: https://tor.meet.coop/gab-dpb-9zt-7tq

1. Each group demo their work.

## Sessions

Sessions held in the June 2022 hack week:

* [Tangible Internet freedom](tanginble-internet-freedom.md)
* [Creating a social media post bank for Tor](social-media-post-bank.md)
* [Modularization of reproducible builds system](modularization-reproducible-build-system.md)
* [Improve support for Pluggable Transports in Tails](improve-support-pt-in-tails.md)
* [Hack on Gosling! #rust #onion_services #network_protocols #testing](gosling.md)
* [Rust: adhoc derive macro facility, for easily autogenerating code from struct definitions](rust.md)
* [Onion Service address discovery using Sauteed Onions](sauteed-onions.md)
* [Gitlab: hack at it](gitlab.md)
* [Tor-Split-Tunneling for Bitmask](bitmask.md)
* [Parse Tor network data and store it in a database](tor-network-data.md)
* DoS attack mitigations - talk with ahf in irc.otfc.net if you want to joint this group.
* [Docs Hackathon](documentation.md)

## Retrospective

We held a [retrospective](retrospective.md) at the end.
