# Documentation

A lot of our documentation (wiki-based and on our support portal) is
incomplete, outdated, or needs to be migrated from an old archived site. The
wiki-based documentation also suffers from being based in gitlab; the only
users that can contribute are users that have write access to the repository.

The outdated docs and the lack of a proper wiki solution have been low priority
enough that nobody's had time to work on them. The hack week is the perfect
time to try and tackle the issues!

## About the project

- Project leader: kez
- Where to meet:
  - IRC: #hackweek-docshackathon-2022 on OFTC IRC
  - BBB: <https://tor.meet.coop/kez-cmz-fe7-lcw>
- Contact :
    - OFTC IRC: kez
    - email: kez@torproject.org

## Team members

    kez (kez on OFTC, or kez@torproject.org)

## How to join the project

- ^ add yourself to this pad in https://pad.riseup.net/p/JR-4Qaa7xUl0eb5WQ6lR#L17
- contact kez on irc.oftc.net

## Skills Needed

Basic familiarity with gitlab and wiki software (as an admin or user) is useful. Being able to update lektor pages is also helpful, though not required! The most important skill is being friendly :)

## Documentation updates

The checklist below is made of tickets from the [Docshackathon tag][]. These are tickets that should be a good starting point for hacking on documentation! If you want to see all the documentation-related tickets, take a look at the [Documentation tag][] (there's a lot though!)

[Docshackathon tag]: <https://gitlab.torproject.org/groups/tpo/web/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Docshackathon&first_page_size=20>

[Documentation tag]: <https://gitlab.torproject.org/groups/tpo/web/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Documentation&first_page_size=20>

- [ ] https://gitlab.torproject.org/tpo/web/tpo/-/issues/151
- [ ] https://gitlab.torproject.org/tpo/web/community/-/issues/127
- [ ] https://gitlab.torproject.org/tpo/web/manual/-/issues/125
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/92
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/244
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/242
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/239
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/232
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/229
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/224
- [ ] https://gitlab.torproject.org/tpo/web/tpo/-/issues/68
- [ ] https://gitlab.torproject.org/tpo/web/manual/-/issues/123
- [ ] https://gitlab.torproject.org/tpo/web/community/-/issues/115
- [ ] https://gitlab.torproject.org/tpo/web/tpo/-/issues/169
- [ ] https://gitlab.torproject.org/tpo/web/manual/-/issues/99
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/20
- [ ] https://gitlab.torproject.org/tpo/web/support/-/issues/90

## Finding a wiki

Because gitlab wikis are restricted to project members with Developer permission <https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/76>, we have to split our wikis out into their own gitlab project that mirrors to the actual gitlab wiki page. it's less than ideal, and creates a huge barrier to entry for people who want to contribute. Let's look at some wiki solutions!

We don't have a list of requirements for a wiki service yet, so for now we can brainstorm ideas here, and at the end of the week write up a [TPA RFC][] to propose a wiki service.

[TPA RFC]: <https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-1-policy>

These are the issues we're running into; potential solutiuons should fix at least some of them, but not necessarily all:
- users must have developer permission to contribute, or open an MR
- documents must be written in a specialized markdown that isn't quite compliant
- no configurability or extensibility: we get what we get

- [Gitit](https://github.com/jgm/gitit)
  - pros:
    - single static binary with only git as a dependency makes deployment simple
    - can export to a lot of different formats
    - allows anonymous users to contribute
    - extensible through plugins
  - cons:
    - using files instead of a full database means limited scaling
    - using git might require a lot of disk-space
    - per-project wikis will require a user to create multiple accounts, unless we use a reverse proxy to set basic-auth before proxying
- [Mediawiki](https://www.mediawiki.org/wiki/MediaWiki)
- [Dokuwiki](https://www.dokuwiki.org/dokuwiki)
- [PhpWiki](https://sourceforge.net/projects/phpwiki/)
- [Twiki](https://twiki.org/)
