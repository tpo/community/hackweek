# Gitlab: hack at it

Project lead: anarcat
Meet room: https://tor.meet.coop/ana-amp-2kq-z2o and #tor-admin in irc.oftc.net
How to join: join the above Big Blue Button room and say hi, ping us on the above IRC channel if we don't respond. Please add yourself to the team list below.

preseantation for the hackweek: https://www.youtube.com/watch?v=NnVnpcYtJ64

## Team

- anarcat
- maybe juga
- eta

Some ideas of GitLab stuff to hack on...

one idea would to progress like this:

 0. lunch (done)
 1. setup a play VM (done!) https://gitlab.torproject.org/tpo/tpa/team/-/issues/40819
 2. install gitlab with puppet, lots of changes required in puppet (done!)
 2. test backup/restore https://gitlab.torproject.org/tpo/tpa/team/-/issues/40820
 3. try setting up postgresql replication not sure that's a good idea, better to have just one time backup / restore for now, I feel like. otherwise the dev server will be unusable - it will keep having conflicts with the prod one?
 4. play: try the stuff below no time, because had to deal with lots of fires:
    * the ganeti cluster crashed: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40816
    * one host probably has to be replaced or repaired https://gitlab.torproject.org/tpo/tpa/team/-/issues/40818
    * we had multiple OOM issues in metrics: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40815 https://gitlab.torproject.org/tpo/tpa/team/-/issues/40814

phew, what a week!

## deploy a dev GitLab server

## deploy postgresql and prometheus servers from our existing infrastructure

https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/20

## enable postgresql replication to turn the dev server into a "warm spare"

## fun Prometheus exporter: "monitor gitlab issues counts in prometheus"

https://gitlab.torproject.org/tpo/tpa/team/-/issues/40591

## "scale out GitLab to 2k users"

https://gitlab.torproject.org/tpo/tpa/team/-/issues/40479

## build container images inside GitLab,

AKA "Assemble kaniko debug image for use in Gitlab CI"

https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/123

## enable Gitlab Container Registry

https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/89

## Large file storage problems

AKA large-scale storage problems brainstorm

https://gitlab.torproject.org/tpo/tpa/team/-/issues/40478
