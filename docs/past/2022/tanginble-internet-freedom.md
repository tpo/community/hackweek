# Tangible Internet Freedom

## About

Hey! Thanks for joining this project :-) Hopefully we can have some fun and create playful exercises. Some info to keep in mind:

    Point of contact: raya (irc) - ask for email and signal # on irc :-)

    First meeting: Tuesday 28 June at 1500 UTC

    Meeting room: https://tor.meet.coop/ray-phn-qyg-ars

    Presentation: https://nc.torproject.net/s/WCzaH8maayqxnoT

    IRC channel: #hw22-tangible

## Team

- Raya
- Maybe Gaba (will not be at the bbb meeting but will check out the project as it goes and how I can help) <3
- Shelikhoo (Part-Time, also joined Gosling) Sick Leave
- Joydeep

## Topics for exercises (will link pads - add suggestions below):
    - Symmetric and asymmetric encryption
    - Metadata and traffic analysis
    - Encyption and end-to-end encryption:
        - https://pad.riseup.net/p/hw22-tangible-encryption
    - Tor onion routing
        -
    - Internet shutdowns and anti censorship tools
    -

## Notes from 28 June 2022:

- look at other internet freedom topics like censorship and internet shutdowns

    - make sure people understand how anti-censorship tools work

    - physical demonstration of the concept

- sometimes when we explain anti censorship tools

    - we have written articles, posts, share them

    - censorship is a cat and mouse game

    - key point: people need to understand how censorship happens

    - india leading in internet shutdowns

    - during riots, government shuts down giving ridiculous excuses

- audience for exercises: public / general, journalists, HRDs
- difficult to design 3d models(for printing) with the time we have

    - either design things with simple material, or do 3d renders and videos

- each person takes on a topic and creates a tangible exercise around it:

    - shell: anti-censorhsip tools

    - joydeep: communication metadata

    - raya: encryption and end-to-end encryption

    - raya: Tor onion routing


## Submitted proposal

### Abstract

In trainings it would be useful to have hands-on exercises that are actually "hands on". The idea is to explain and illustrate technical concepts related to Internet freedom topics via tangible exercises that include 3D printed objects, laser cut objects, paper material, etc.

### Description

The objective is to design a set of playful exercises that digital security and Tor trainers can make use of in training sessions to aid in the teaching of technical concepts related to Internet freedom. Concepts can include: Tor onion routing, end-to-end encryption, communication metadata, Internet shutdowns, and more.

All exercises and corresponding design files will be uploaded to a webpage or GitLab project or group... we'll see :-) The idea is to make them as open as possible so folks can make use of them and adapt in their trainings, but also to collect feedback on the exercises, and perhaps allow for localization!

This is inspired by many projects of the sort including Crypto Santa (secret santa + onion routing) and Venezuela Inteligente's 3D-printed "Asymmetric Lock Box": https://github.com/VEinteligente/asymentric-lock-box/

### Skills needed

- designing skills, 2D graphics and/or 3D models
- documentation skills
- access to fabrication tools, maybe a makerspace with laser cutters and 3D printers?
- generally enjoy brainstorming
