# Hackweek 2022 - Retrospective

A retrospective is an activity where we reflect together about what worked and didn't work in a specific project.

The goal of this retrospective is to learn and fix problems, not blaming.

For this retrospective let's focus on the hackweek we had in the last week of June 2022.

Add as many items to each list as you think you need to add.

## +1 What worked well

* [x] having less meetings, time to focus on projects we normally wouldn't have +1+1+1+1+1
* [x] getting to work with people you may not normally interact with as much +1+1+1+1+1+1
* [x] opportunity for a team work together in the same project (almost) synchronously +1
* [x] had a lot of fun +1+1+1+1+1

## -1 What didn’t work well? What were the challenges?

* [~] still had meetings, ended up working more hours than normal
* [~] worked too much to achieve the proposed goals (a single week is not enough!) +1+1
* [~] had emergencies to deal with which made it hard to accomplish goals +1+1+1+1
* [~] hard to onboard people from the outside on some projects +1
* [~] others worked in their hack project on regular work which is relevant to my day-to-day work as well and I struggled to keep an eye on that work while trying to focus on my hackweek work (and emergencies...) +1

## What could we try to do differently?

* [ ] schedule it so it's not the last week of the quarter +1+1+1+1
* [ ] publicize it more outside of mailing lists to bring in more outside/new contributors
* [ ] maybe have less latency between hackweek proposals getting submitted and posted to the "schedule"?
