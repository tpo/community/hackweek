# Creating a social media post bank for Tor

## Project Abstract

In this project, we will write and create a bank of social media content for the Tor Project's social media channels.

## Project Description

The goal of this project is to create a bank of varied social media content that can be easily sorted and/or searched in some meaningful way. The intention is to help the Communications team be able to easily find posts on certain topics or themes, and to potentially provide this content to the localization team such that we can post in a variety of languages.

## What are the skills required in your team to work on this project?

- English as a written language
- Interest in creating social media content
- General knowledge about Tor, but you do not need to be highly technical
- Ability to provide and receive constrictive editing
- Creativity!

## How to join the project

Message alsmith on the tor-project IRC channel

    or

Email smith@torproject.org

## Team (ADD YOURSELF IF YOU WANT TO JOIN!)
    al
    isabela
    nico b
    emmapeel
    nickm a little bit

Meeting with the team
Room: https://tor.meet.coop/al--7ez-cjq-vrh

Monday, 17:00 UTC kick off with the team
Tuesday, 17:00 UTC check-in  MOVED TO 16:30 TODAY
Wednesday, no scheduled meeting, but we can meet in the team room any time after 21:00 UTC
Thursday, 16:00 UTC check-in
*Check-ins are as-needed

Things that need to be determined
What's the best text-based channel for folks to discuss?

Things to do this week
- Write out list different categories of kinds of social post content (like, "call to action," or "how-to," or "memes," or "did you know")
- Begin writing content in each of the defined categories

    - Use content that already exists and repurpose!!+1+1

- Define what's needed in a tool that can act as a bank / what would be useful (e.g., tagging, searching)
- Is there someone at Tor who knows a tool that would work for us, or who could spin up an instance of something for the comms team long term? maybe we should sneak it onto the tpo website, so it gets translated onto the most active locales? emmapeel
- Connect w/ emmapeel & brainstorm how best to prepare this content for localization--what would a pipeline look like?see above ^^

=====================================================================================================

content repo now lives here: https://gitlab.torproject.org/tpo/web/social-bank

=====================================================================================================

Content Categories:

CALL TO ACTION

    - become a ux volunteer researcher

    - become a localizer

    - become an alpha tester

    - subscribe to newsletter

    - run a tor relay

    - run a tor bridge

    - become a snowflake


- how to / i guess they should come with a link?

    - protect your visitors privacy when you develop a website

    - protect yourself while buying online

    - get out of google

    - use snowflake - as a bridge transport, or as a plugin

    - maybe: how running a snowflake is different than using it as a circumvention tool+1

    - mantain a tor relay

    - overloaded relay -

    - look at tor metrics



DID YOU KNOW

    - N users connected yesterday from $country

    -BLOCK TRACKERS "Tor Browser isolates each website you visit so third-party trackers and ads can't follow you. Any cookies automatically clear when you're done browsing. So will your browsing history."

    -DEFEND AGAINST SURVEILLANCE "Tor Browser prevents someone watching your connection from knowing what websites you visit. All anyone monitoring your browsing habits can see is that you're using Tor."

    - RESIST FINGERPRINTING "Tor Browser aims to make all users look the same, making it difficult for you to be fingerprinted based on your browser and device information."

    - MULTI-LAYERED ENCRYPTION "Your traffic is relayed and encrypted three times as it passes over the Tor network. The network is comprised of thousands of volunteer-run servers known as Tor relays."


    thinking about newer/not-yet users who we might want to reach, 'why you should care about privacy, why you should use tor, why you should use a vpn, etc'


We believe everyone should be able to explore the internet with privacy.


We advance human rights and defend your privacy online through free software and open networks.


Defend yourself against tracking and surveillance. Circumvent censorship.




MEMES

    - use the right onion for the job (pic of different onions and how to cook them; add tor to the graphic)





FUNDRAISING





CALENDAR OF EVENTS

Nick's timeline of events, Nick notes that we need to verify all of these dates before we decide to use them: https://pad.riseup.net/p/timeline-of-tor-stuff


January

15 January - Wikipedia's birthday

24-28 January - Data Privacy Week
https://www.fpc.gov/data-privacy-week-2022/

February

01-28 February - Black History Month

2nd Tuesday February - Safer Internet Day

11 February - International Day of Women and Girls in Science

20 February - World Day of Social Justice

March

8 March - International Women's Day

21 March - International Day for the Elimination of Racial Discrimination

22 March - World Water Day

24 March - International Day for the Right to the Truth concerning Gross Human Rights Violations and for the Dignity of Victims

25 March - International Day of Remembrance of the Victims of Slavery and the Transatlantic Slave Trade

April

05 April - International Day of Conscience
About: "Promoting a culture of peace with love and conscience" https://www.un.org/en/observances/conscience-day

07 April - World Health Day
About: Each year draws attention to a specific health topic

22 April - Earth Day

28 April - World Day for Safety and Health at Work
About: Promotes the prevention of occupational accidents and diseases globally. New and emerging occupational risks may be caused by technical innovation or by social or organizational change, such as: New technologies and production processes, e.g. nanotechnology, biotechnology, https://www.un.org/en/observances/work-safety-day

May

01 May - International Workers' Day (May Day)

immigrants rights - i couldn't find an immigrants rights day in may, help!

03 May - World Press Freedom Day
https://www.un.org/en/observances/press-freedom-day

    celebrate the fundamental principles of press freedom;

    assess the state of press freedom throughout the world;

    defend the media from attacks on their independence;

    and pay tribute to journalists who have lost their lives in the line of duty.


31 May - Anniversary (1996) of the first public presentation of onion routing in Cambridge, UK at Isaac Newton Institute's first Information Hiding Workshop. (Aka, onion routing's birthday!)

    First Onion Routing paper: https://www.onion-router.net/Publications.html#IH-1996

    Great images here: https://www.onion-router.net/Publications/Briefing-1996.pdf

    PrivChat celebrating 25th annivesary here: https://www.torproject.org/privchat/chapter-4


June
01-30 - Pride Month

04 June - Anniversary of the Tiananmen Square massacre (1989)

05 June - Snowden revelation day (2013)

06 June - Russian Language Day

19 June - Juneteenth

28 June - Anniversary of Stonewall Riots (1970)

July

August

16 August - Tails' birthday! (2009)

September

15 September - International Day of Democracy

28 September - International Day for Universal Access to Information

30 September - International Translation Day

    Our community of volunteers, our friends at @LocalizationLab, and our donors make the localized versions of Tor tools and documentation. Today we celebrate International Translation Day by saying THANK YOU to everyone who has contributed to localizing our tools and making Tor accessible to everyone. :purple heart:


October

01-31 October - Cybersecurity Awareness Month
About: https://www.cisa.gov/cybersecurity-awareness-month

22 October - Global Encryption Day
About: https://www.globalencryption.org/2022/06/global-encryption-day-2022/

29 October - Internet Day
About: https://theinternetday.com/

November

8 November - Aaron Swartz's birthday (1986)

25 November - 10 December: 16 days of activism against gender based violence  https://www.unwomen.org/en/what-we-do/ending-violence-against-women/take-action/16-days-of-activism

December

10 December - Human Rights Day



RELEASE TEMPLATES

- tor
:green circle: Relay, Exit, and Bridge operators, it's time to update: Tor [VERSION NUMBER] is now available. This version includes [THE FOLLOWING UPDATES].
<link> (dgoulet posts only on the forum, no blog)

- arti
:green circle: Arti [VERSION NUMBER] is now available! Arti is our ongoing project to create a working embeddable Tor client in Rust. This release adds [THE FOLLOWING UPDATES].
<link>

- tor browser stable
:purple circle: Time to update: Tor Browser [VERSION NUMBER] (LIST OF PLATFORMS) is now available. This release includes [THE FOLLOWING UPDATES] and updates Firefox on [LIST OF PLATFORMS] to [ESR VERSION]
<link>

- tor browser alpha
(1/2) :white circle: New Alpha Release: Tor Browser [VERSION NUMBER] (LIST OF PLATFORMS) is now available. This release includes [THE FOLLOWING UPDATES] and updates Firefox on [LIST OF PLATFORMS] to [ESR VERSION].
<link>

(2/2) Tor Browser Alpha is an early version of Tor Browser. As a nonprofit, we rely on volunteer testers to test these releases. Help at-risk and censored users safely and easly access Tor by testing this Alpha release. Here's how:
https://blog.torproject.org/vounteer-as-an-alpha-tester/

- snowflake, onionoo, OONI, etc
Where do I find Snowflake release info?
Where do I find onionoo release info?
Where do I find OONI release info?

- tails
:laptop computer: Tails [VERSION NUMBER] is now available from @tails_live. This release includes [THE FOLLOWING UPDATES].
<link>

- secure drop
:open file folder: SecureDrop [VERSION NUMBER] is now available from @SecureDrop. This release includes [THE FOLLOWING UPDATES].
<link>

EXISTING TWEETS IN OTHER LANGUAGES

如何在中国大陆绕过 GFW 防火长城连接到 Tor？

中国大陆的用户可能需要一些额外步骤来绕过防火长城，连接到 Tor 网络。
https://forum.torproject.net/t/gfw-tor/3447
