# Hackweek 2023

## About

* [Hackweek 2023 announcement message][].
* [Hackweek 2023 milestone][] lists all proposals/projects/tasks.
* Tor's [User Documentation Guidelines][]. For improvements and other feedback
  about the User Documentation Style Guide, please open a ticket on
  [tpo/community/support][]. For fixing typos and addressing other minor issues,
  please feel free to directly edit the wiki page.

[Hackweek 2023 announcement message]: https://lists.torproject.org/pipermail/tor-project/2023-August/003675.html
[tpo/community/support]: https://gitlab.torproject.org/tpo/community/support/
[User Documentation Guidelines]: https://gitlab.torproject.org/tpo/team/-/wikis/Tor-User-Documentation-Style-Guide
[Hackweek 2023 milestone]: https://gitlab.torproject.org/tpo/community/hackweek/-/milestones/1

## Projects

* rhatto: [Onion MkDocs tryout](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13)
* roger and joydeep: [Clean up and improve the user support FAQ text](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/17)
* gaba: [Working on the content for the developer portal](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/18)
  (this project didn't get picked up; gaba selected other project to work on)
* nickm: [Cleanups and improvements on Tor specifications](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/19)
    * [Link 1](https://spec.torproject.org/)
    * [Link 2](https://spec.torproject.org/intro/index.html)
    * [Link 3](https://spec.torproject.org/tor-spec/negotiating-channels.html)
* onyinyang: [Improve Lox Documentation](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/20)
* gabi-250: [Arti key manager documentation](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/22)
* geko & juga: [Fix missing documentation in network-health land](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/23)
  (this project got picked up!)
* smith: [Public documentation about project design and grant writing process](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/24)
  (i made mild progress but mostly had other, time-sensitive tasks to deal with)
* pierov: [tor-browser-build project survey](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/25)
* anarcat: [wiki replacement considerations](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/26)
  (just go down to the last comment for the "presentation")
* anarcat: [TPA issue templates](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/27)
  (this project didn't get picked up)
* gaba: [Public documentation about how we manage projects at Tor](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/28)
* boklm: [Document how to verify reproducibility of build of a mullvad/tor browser release](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/29)
* donuts: [design.torproject.org](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/30)
* emmapeel: [Tweets for the Comms team](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/31)
* rhatto [Onion TeX Slim enhancements](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/14) (actually happened)
* rhatto / juga: [Onion Reveal coding and documenting](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15)
* micah [Collaborative editing](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/16):
    * Proposed goals/rubric for evaluating collaborative editing tools, please provide feedback!
    * Reducing the overall number of tools we have to deal with
    * Integrating with our existing tools, so things are less detached
    * Bridging the gap between ephemeral pads and the need for information permanence
    * Achieving parity with the functionality/features that we like from existing pads
    * Meets the needs of the entire organization
    * Useful access restrictions (public vs. private, vandalism, etc.)
    * Less fragile
* rhatto: [Spell checker CI for Markdown (and maybe other) files](https://gitlab.torproject.org/tpo/community/hackweek/-/issues/21)
  (didn't get picked up)
* richard: Tor Browser threat documentation (didn't see this in GitLab)

## Retrospective

### What went well?

* [x] We experimented with collaborative editors, that was fun!
* [x] Lots of work on mkdocs, the wikipelago is impressive.
* [x] This week I've already had to deal with stuff I remembered of only because
      the previous week I documented it.
* [x] Writing documentation made me think about some stuff we might be doing in the
      wrong way and we could do better.
* [x] Re-writing old docs made me realize:
    * [x] How some logic/software has changed.
    * [x] How some logic/software is still the same.
    * [x] Some topics we haven't really solved yet.
    * [x] Some topics I had forgotten about.
* [x] Was pretty intense and fun!
* [x] It makes a lot of difference to have full time allocated into a single project +1
* [x] Really nice to do hands-on work with people I don't usually work with +1
* [x] Using tickets to coordinate the proposals seemed to be easier than with the
      approach from previous years +1

### What didn't work well?  What were the challenges?

* [ ] TPA had to deal with a major emergency and couldn't complete its projects.
* [ ] Only writing docs for a few days feels more tiring than usual work to me :D
      (lots of details to write, they required a constatly high level of focus not
      to miss them).
* [ ] There is always something happening dragging me from focusing fully on the
      hackweek, or maybe just me procastinating hard.
* [ ] Squeezing lots of documentation demands in a single week was rewarding in the
      end, but very challenging and tiresome.
* [ ] There were many more existing gitlab backlog issues than we anticipated -- we
      had in mind to spend the week discovering new issues and opening new tickets,
      but we mostly spent the week working on already-filed issues.

### What could we try to do differently?

* Defining the Hackweek date more in advance.

## Questions

* A bit unrelated: has MkDocs a way to show as "source" markdown files in
  Nextcloud (no GitLab)?
