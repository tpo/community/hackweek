# Pre-Hackweek activities

0. Check that there is no release happening that week.

1. Review the existing process.

2. Present the Hackweek at the All Hands 2 months earlier.
  * Present process and where we are going to add proposals.

3. Define the public place for folks to add the projects they want to do during
   the hackweek, like the existing [issue queue][]. Recommend the use of the
   [proposals template][].

4. Once we have the above, we should send an email out to everyone at Tor
   (tor-internal) telling people to start adding their ideas to the page. Give
   people 2 to 3 weeks to add projects. Use the [CfP message template][]
   as a model.

5. Call for help: We keep this going for a few weeks, when we decide that we
   have enough projects listed there we can start the 'call for help' so teams
   can get the help they need. Send mail to tor-project@ and otf-talk@ and
   publish in teams pads but do not post in social media.

[issue queue]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues
[proposals template]: https://gitlab.torproject.org/tpo/community/hackweek/-/blob/main/.gitlab/issue_templates/Default.md
[CfP message template]: cfp.md
