# How To Organize a Hackweek

* [Pre-hackweek organization](pre.md).
* [Call For Proposals Message Template](cfp.md).
* [Hackweek Process](process.md).
* [Post-hackweek activities](post.md).
