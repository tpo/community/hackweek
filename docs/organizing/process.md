# Hackweek Process

## Timeline

* Until the first day of the Hackweek:
  * Send project proposals to the [issue queue][] (please use the [proposal
    issue template][] for the ticket Description).

  * Before hackweek begins, start looking for other people to join
    your team.

  * In order to join a proposal you liked, explicitly make a comment on it's
    ticket.

* One week before: All-Hands session prior to the Hackweek where:
  * People/teams will present their project proposals for other people to join
    their team if they want to.

  * Logistics will be discussed, like whether we'll will have a main pad with
    all information and links to team's pads etc.

* First day: Hackweek begins. Each project team kick off their work on the
  pre-hackweek meeting.  People start working on whatever they want related to
  documentation. By this time, you should have a few members of your team
  already identified. Hack hack hack hack... in whatever way you organize
  yourself!

* Last day: Hackweek ends. All projects should be done by the last day.

* A week after: each team presents the work they did in the All-Hands session
  happening after the Hackweek:
    * It will be recorded to upload to Youtube later.

[proposal issue template]: https://gitlab.torproject.org/tpo/community/hackweek/-/blob/main/.gitlab/issue_templates/Default.md
[issue queue]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues

## Locations

1. We can provide a teleconferece room for each team to meet and hang out as
   they work.
2. We can provide a a teleconference "lounge" space for people to drop in
   during the week in case there are questions or comments, etc.
3. We will have the room #tor in irc.oftc.net to discuss general hackweek
   things.

## Management

* When filing issues, use a GitLab milestone for each Hackweek edition,
  so all proposals stay organized.
