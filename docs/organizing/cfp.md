# Call For Proposals

This is a template for the call for proposals message that can be adapted (like
in this [example][]) case by case:

```markdown
Hi!

The Tor Project and Tor community is going to be gathering online from
`$hackweek_begin` to `$hackweek_end` this year for a `$n` days hackweek.

## About

This is a call for projects for whoever wants to participate, put together a
team and hack through one working week with us. In the context of this
hackweek, a project is anything related to `$topic_description` that you can
work with other people in 4 days.

This is an opportunity to discuss `$discussion_themes` , as well as thinking,
proposing, researching and testing solutions.

It could be:

* `$example_1`
* `$example_2`
* ...
* `$example_n`

## Process

...

## Timeline

Insert timeline here, according to the [process](PROCESS.md).

## Locations

Insert locations here, according to the [process](PROCESS.md).

## Projects

The updated list of projects will be available at
https://gitlab.torproject.org/tpo/community/hackweek/-/issues.

Each project can have one pad (you can use https://pad.riseup.net) and also use
it's ticket to add all information that people need to add themselves to that
project.
```

[example]: https://lists.torproject.org/pipermail/tor-project/2023-August/003675.html
